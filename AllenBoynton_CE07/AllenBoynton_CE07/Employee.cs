﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Employee : IComparable
    {
        private string _name;
        private string _address;

        public Employee(string name, string address)
        {
            _name = name;
            _address = address;
        }

        public string Name { get => _name; set => _name = value; }
        public string Address { get => _address; set => _address = value; }

        // Implement IComparable CompareTo method - provide default sort order
        public int CompareTo(object obj)
        {
            Employee employeeName = (Employee)obj;

            return String.Compare(this.Name, employeeName.Name);
        }

        public virtual decimal CalculatePay
        {
            get
            {
                return 0m;
            }
        }
    }
}
