﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Hourly : Employee
    {
        private decimal _payPerHour;
        private decimal _hoursPerWeek;

        public Hourly(string name, string address, decimal payPerHour, decimal hoursPerWeek) : base(name, address)
        {
            _payPerHour = payPerHour;
            _hoursPerWeek = hoursPerWeek;
        }

        public decimal PayPerHour { get => _payPerHour; set => _payPerHour = value; }
        public decimal HoursPerWeek { get => _hoursPerWeek; set => _hoursPerWeek = value; }

        public override decimal CalculatePay => PayPerHour * HoursPerWeek * 52;
    }
}
