﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Manager : Salaried
    {
        private decimal _bonus;

        public Manager(string name, string address, decimal salary, decimal bonus) : base(name, address, salary)
        {
            _bonus = bonus;
        }

        public decimal Bonus { get => _bonus; set => _bonus = value; }

        public override decimal CalculatePay => Salary + _bonus;
    }
}
