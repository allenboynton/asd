﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Validation
    {
        // Validation for user's name entry
        public static string GetString(string message)
        {
            // Variable to store user's entry
            string validatedString = null;

            // Do/while loop to check entry is not left blank
            do
            {
                Console.Write(message);
                validatedString = Console.ReadLine();
                
            } while (string.IsNullOrWhiteSpace(validatedString));

            return validatedString;
        }

        // Validation for the number entered
        public static int GetInt(string message = "\nEnter a valid number: ")
        {
            // Number variable to store the parsed user's entry
            int validatedInt;
            string input = null;

            // Do/while loop to check entry is a number
            do
            {
                Console.Write(message);
                input = Console.ReadLine();
                
            } while (!Int32.TryParse(input, out validatedInt));

            return validatedInt;
        }

        public static decimal GetDecimal(string message = "\nEnter a valid number: ")
        {
            // Number variable to store the parsed user's entry
            decimal validatedDecimal;
            string input = null;

            // Do/while loop to check user entry is a valid number
            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!decimal.TryParse(input, out validatedDecimal));

            return validatedDecimal;
        }
    }
}
