﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> currentEmployees = new List<Employee>();
            
            Program instance = new Program();

            bool programIsRunning = true;

            while (programIsRunning)
            {
                // Menu per exercise requirements
                Console.Clear();
                Console.WriteLine("Main Menu\n" +
                    "1. Add Employee\n" +
                    "2. Remove Employee\n" +
                    "3. Display Payroll\n" +
                    "4. Exit");
                Console.Write("Select an option: ");
                string input = Console.ReadLine().ToLower();

                // Switch statement recognizes which choice is picked by the user
                switch (input)
                {
                    case "1":
                    case "add employee":
                        {
                            instance.AddEmployee(currentEmployees);
                        }
                        break;
                    case "2":
                    case "remove employee":
                        {
                            instance.RemoveEmployee(currentEmployees);
                        }
                        break;
                    case "3":
                    case "display payroll":
                        {
                            instance.DisplayPayroll(currentEmployees);
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            // Program is exited
                            programIsRunning = false;
                        }
                        break;
                }

                Utility.DisplayMenuBeforeExiting();
            }
        }

        private Employee AddEmployee(List<Employee> currentEmployees)
        {
            Employee newEmployee = null;
            string name = null;
            string address = null;
            decimal payPerHour = 0m;
            decimal hoursPerWeek = 0m;
            decimal salary = 0m;
            
            bool selectingEmployeeType = true;

            while (selectingEmployeeType)
            {
                Console.Write(
                    "\nAdd Employee\n" +
                    "1. Full Time\n" +
                    "2. Part Time\n" +
                    "3. Contractor\n" +
                    "4. Salaried\n" +
                    "5. Manager\n" +
                    "6. Previous Menu\n" +
                    "Select an option: ");
                string type = Console.ReadLine().ToLower();
                
                switch (type)
                {
                    case "1":
                    case "full time":
                        {
                            name = Validation.GetString("\nEmployee Name: ");
                            address = Validation.GetString("Employee Address: ");
                            payPerHour = Validation.GetDecimal("Pay per hour: ");

                            newEmployee = new FullTime(name, address, payPerHour);

                            currentEmployees.Add(newEmployee);

                            selectingEmployeeType = true;
                        }
                        break;
                    case "2":
                    case "part time":
                        {
                            name = Validation.GetString("\nEmployee Name: ");
                            address = Validation.GetString("Employee Address: ");
                            payPerHour = Validation.GetDecimal("Pay per hour: ");
                            hoursPerWeek = Validation.GetDecimal("Hours per week: ");

                            newEmployee = new PartTime(name, address, payPerHour, hoursPerWeek);
                            
                            currentEmployees.Add(newEmployee);

                            selectingEmployeeType = true;
                        }
                        break;
                    case "3":
                    case "contractor":
                        {
                            name = Validation.GetString("\nEmployee Name: ");
                            address = Validation.GetString("Employee Address: ");
                            payPerHour = Validation.GetDecimal("Pay per hour: ");
                            hoursPerWeek = Validation.GetDecimal("Hours per week: ");
                            decimal noBenefitsBonus = Validation.GetDecimal("Bonus: ");

                            newEmployee = new Contractor(name, address, payPerHour, hoursPerWeek, noBenefitsBonus);
                            
                            currentEmployees.Add(newEmployee);

                            selectingEmployeeType = true;
                        }
                        break;
                    case "4":
                    case "salaried":
                        {
                            name = Validation.GetString("\nEmployee Name: ");
                            address = Validation.GetString("Employee Address: ");
                            salary = Validation.GetDecimal("Salary: ");

                            newEmployee = new Salaried(name, address, salary);
                            
                            currentEmployees.Add(newEmployee);

                            selectingEmployeeType = true;
                        }
                        break;
                    case "5":
                    case "manager":
                        {
                            name = Validation.GetString("\nEmployee Name: ");
                            address = Validation.GetString("Employee Address: ");
                            salary = Validation.GetDecimal("Salary: ");
                            decimal bonus = Validation.GetDecimal("Bonus: ");

                            newEmployee = new Manager(name, address, salary, bonus);
                            
                            currentEmployees.Add(newEmployee);

                            selectingEmployeeType = true;
                        }
                        break;
                    case "6":
                    case "previous menu":
                        {
                            selectingEmployeeType = false;
                        }
                        break;
                }
            }

            currentEmployees.Sort();

            return newEmployee;
        }

        private void RemoveEmployee(List<Employee> currentEmployees)
        {
            if (currentEmployees.Count == 0)
            {
                Console.WriteLine("\nNeed to add an employee first.\n");
            }
            else
            {
                bool removeEmployee = true;

                while (removeEmployee)
                {
                    Console.WriteLine("\nRemove Employee");
                    
                    int i = 1;
                    foreach (Employee employee in currentEmployees)
                    {
                        Console.WriteLine($"{i++}. {employee.Name}");
                    }

                    Console.WriteLine($"{currentEmployees.Count + 1}. Exit");
                    int type = Validation.GetInt("Make a selection: ");
                    
                    if (type == currentEmployees.Count + 1)
                    {
                        removeEmployee = false;
                    }
                    else
                    {
                        currentEmployees.RemoveAt(type - 1);

                        removeEmployee = true;
                    }
                }
            }
        }

        private void DisplayPayroll(List<Employee> currentEmployees)
        {
            if (currentEmployees.Count == 0)
            {
                Console.WriteLine("\nNeed to add an employee first.\n");
            }
            else
            {
                Console.Clear();
                Console.WriteLine(
                        $"ABC Company Payroll\n" +
                        $"Name:\t\tAddress:\t\tYearly Income:\n" +
                        $"-----\t\t--------\t\t--------------");

                foreach (Employee employee in currentEmployees)
                {
                    if (employee is Employee)
                    {
                        Employee temp = employee as Employee;

                        Console.WriteLine(
                            $"{employee.Name}\t\t{employee.Address}\t\t" +
                            $"{(temp.CalculatePay).ToString("C")}");
                    }
                }
            }
        }
    }
}
