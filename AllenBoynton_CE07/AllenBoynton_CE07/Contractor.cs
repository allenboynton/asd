﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Contractor : Hourly
    {
        private decimal _noBenefitsBonus;

        public Contractor(string name, string address, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitsBonus) : base(name, address, payPerHour, hoursPerWeek)
        {
            _noBenefitsBonus = noBenefitsBonus;
        }

        public decimal NoBenefitsBonus { get => _noBenefitsBonus; set => _noBenefitsBonus = value; }

        public override decimal CalculatePay => (PayPerHour * HoursPerWeek + NoBenefitsBonus) * 52;
    }
}
