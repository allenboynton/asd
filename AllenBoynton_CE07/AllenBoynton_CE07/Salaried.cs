﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE07
{
    class Salaried : Employee
    {
        private decimal _salary;

        public Salaried(string name, string address, decimal salary) : base(name, address)
        {
            _salary = salary;
        }

        public decimal Salary { get => _salary; set => _salary = value; }

        public override decimal CalculatePay => _salary;
    }
}
