﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace AllenBoynton_CE09
{
    class Program
    {
        MySqlConnection connection = null;
        
        static void Main(string[] args)
        {
            List<DVD> inventory = new List<DVD>();
            List<DVD> shoppingCart = new List<DVD>();
            DVD currentDVD = null;

            string title;
            string priceString;
            string ratingString;

            float rating = 0f;
            decimal price = 0m;
            
            Program instance = new Program
            {
                connection = new MySqlConnection()
            };
            instance.Connect();

            DataTable data = instance.QueryDB("SELECT DVD_Title, Price, publicRating FROM dvd LIMIT 20");
            DataRowCollection rows = data.Rows;

            foreach (DataRow row in rows)
            {
                title = row["DVD_Title"].ToString();
                priceString = row["Price"].ToString();
                ratingString = row["publicRating"].ToString();

                while ((!float.TryParse(ratingString, out rating) ||
                    (!decimal.TryParse(priceString, out price))))
                {
                    Console.WriteLine("Error converting Numbers.");
                }

                currentDVD = new DVD(title, price, rating);

                inventory.Add(currentDVD);
            }

            instance.connection.Close();

            bool programIsRunning = true;

            while (programIsRunning)
            {
                Console.Clear();
                DisplayMenu();
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "view inventory":
                        {
                            ViewInventory(inventory);
                        }
                        break;
                    case "2":
                    case "view shopping cart":
                        {
                            ViewShoppingCart(shoppingCart);
                        }
                        break;
                    case "3":
                    case "add dvd to shopping cart":
                        {
                            AddDvdToCart(inventory, shoppingCart);
                        }
                        break;
                    case "4":
                    case "remove dvd from shopping cart":
                        {
                            RemoveDvdFromCart(shoppingCart, inventory);
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Console.WriteLine("\n**** Have a nice day :) ****\n");
                            programIsRunning = false;
                        }
                        break;
                    default:
                        Console.WriteLine("Command not recognized.");
                        break;
                }
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void DisplayMenu()
        {
            Console.WriteLine(
                "DVD Emporium\n" +
                "1. View inventory\n" +
                "2. View shopping cart\n" +
                "3. Add DVD to shopping cart\n" +
                "4. Remove DVD from shopping cart\n" +
                "5. Exit");
            Console.Write("Select an Option: ");
        }

        private static void ViewInventory(List<DVD> inventory)
        {
            Console.Clear();
            Console.WriteLine("DVD Emporium Inventory: ");
            foreach (DVD dvd in inventory)
            {
                Console.WriteLine(dvd);
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void ViewShoppingCart(List<DVD> shoppingCart)
        {
            Console.Clear();
            Console.WriteLine("Shopping cart contents: ");

            foreach (DVD cart in shoppingCart)
            {
                Console.WriteLine(cart);
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void AddDvdToCart(List<DVD> inventory, List<DVD> shoppingCart)
        {
            bool addDVD = true;

            while (addDVD)
            {
                Console.Clear();
                Console.WriteLine("Add a DVD Selection: ");

                int j = 1;
                foreach (DVD dvd in inventory)
                {
                    Console.WriteLine($"{j}. {dvd.Title}");
                    j++;
                }

                Console.WriteLine($"{inventory.Count + 1}. Exit");
                int type = Validation.GetInt("Make a selection: ");

                if (type == inventory.Count + 1)
                {
                    addDVD = false;
                }
                else
                {
                    shoppingCart.Add(inventory[type - 1]);
                    inventory.Remove(inventory[type - 1]);

                    addDVD = true;
                }
            }
        }

        private static void RemoveDvdFromCart(List<DVD> shoppingCart, List<DVD> inventory)
        {
            bool removeDVD = true;

            while (removeDVD)
            {
                Console.Clear();
                Console.WriteLine("Remove DVD from shopping cart: ");

                int j = 1;
                foreach (DVD dvd in shoppingCart)
                {
                    Console.WriteLine($"{j}. {dvd.Title}");
                    j++;
                }

                Console.WriteLine($"{shoppingCart.Count + 1}. Exit");
                int type = Validation.GetInt("Make a selection: ");

                if (type == shoppingCart.Count + 1)
                {
                    removeDVD = false;
                }
                else
                {
                    inventory.Add(shoppingCart[type - 1]);
                    shoppingCart.Remove(shoppingCart[type - 1]);

                    removeDVD = true;
                }
            }
        }

        void Connect()
        {
            BuildConnectionString();

            try
            {
                connection.Open();
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + connection.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username/password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }

                Console.WriteLine(msg);
            }
        }

        void BuildConnectionString()
        {
            string ip = "";

            using (StreamReader sr = new StreamReader(@"c:\VFW\connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string connString = $"server={ip};";
            connString += "uid=dbsAdmin;";
            connString += "pwd=password;";
            connString += "database=exampleDatabase;";
            connString += "port=8889";

            connection.ConnectionString = connString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }
    }
}
