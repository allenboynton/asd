﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace AllenBoynton_CE09
{
    class DVD
    {
        private string _title;
        private decimal _price;
        private float _rating;

        public DVD(string title, decimal price, float rating)
        {
            _title = title;
            _price = price;
            _rating = rating;
        }
        
        public override string ToString()
        {
            string retValue = "";
            retValue += ($"DVD: {Title}\t");
            retValue += ($"Price: ${Price}\t");
            retValue += ($"Rating: {Rating}");

            return retValue;
        }
        
        public string Title { get => _title; set => _title = value; }
        public decimal Price { get => _price; set => _price = value; }
        public float Rating { get => _rating; set => _rating = value; }
    }
}
