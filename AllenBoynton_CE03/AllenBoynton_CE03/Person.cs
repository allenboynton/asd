﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE03
{
    class Person
    {
        // Create member variables
        private string _name;
        private string _description;
        private int _age;

        // Encapsulate fields
        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }
        public int Age { get => _age; set => _age = value; }

        // Create constructor
        public Person(string name, string description, int age)
        {
            _name = name;
            _description = description;
            _age = age;
        }
    }
}
