﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE03
{
    class Program
    {
        static void Main(string[] args)
        {
            Course currentCourse = null;
            Program instance = new Program();
            bool programIsRunning = true;

            while (programIsRunning)
            {
                // Menu per exercise requirements
                Console.Clear();
                Console.WriteLine("Select an action:\n" +
                    "1. Create Course\n" +
                    "2. Create Teacher\n" +
                    "3. Add Students\n" +
                    "4. Display Course\n" +
                    "5. Exit");
                Console.Write("Select an option: ");
                string input = Console.ReadLine().ToLower();

                // Switch statement recognizes which choice is picked by the user
                switch (input)
                {
                    case "1":
                    case "create course":
                        {
                            // Creates currentCustomer
                            currentCourse = instance.CreateCourse();
                        }
                        break;
                    case "2":
                    case "create teacher":
                        {
                            // Instance creates account and balance
                            instance.SetTeacher(currentCourse);
                        }
                        break;
                    case "3":
                    case "add students":
                        {
                            // Instance creates balance
                            instance.AddStudents(currentCourse);
                        }
                        break;
                    case "4":
                    case "display course":
                        {
                            // Instance gets account info
                            instance.DisplayCourseInfo(currentCourse);
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            // Program is exited
                            programIsRunning = false;
                        }
                        break;
                }

                Utility.DisplayMenuBeforeExiting();
            }
        }

        private Course CreateCourse()
        {
            Course newCourse = null;
            Student numStudents = null;

            string title = Validation.GetString("\nEnter the course title: ");
            string description = Validation.GetString("Enter a course description: ");
            int student = Validation.GetInt("Enter number of students in the class: ");

            newCourse = new Course(title, description, numStudents);

            return newCourse;
        }

        private void SetTeacher(Course currentCourse)
        {
            if (currentCourse == null)
            {
                Console.WriteLine("Please create a course first.");
            }
            else
            {
                // If customer was created, user is prompted for account info
                string name = Validation.GetString("\nEnter the teacher's name: ");
                string description = Validation.GetString("Enter the teacher's description: ");
                int age = Validation.GetInt("Enter the age of the teacher: ");
                string[] knowledge = new string[] { Validation.GetString("Enter 3 topics " +
                    "(separated by a ',') the teacher is knowledgable about: \n") };

                // If account info is null, set info 
                if (currentCourse.Teacher == null)
                {
                    currentCourse.Teacher = new Teacher(name, description, age)
                    {
                        Knowledge = knowledge
                    };
                }
                else
                {
                    currentCourse.Teacher.Name = name;
                    currentCourse.Teacher.Description = description;
                    currentCourse.Teacher.Knowledge = knowledge;
                    currentCourse.Teacher.Age = age;
                }
            }
        }

        private void AddStudents(Course currentCourse)
        {
            if (currentCourse == null)
            {
                Console.WriteLine("Please create a course first.");
            }
            else
            {
                // If customer was created, user is prompted for account info
                string name = Validation.GetString("\nEnter the student's name: ");
                string description = Validation.GetString("Enter the student's description: ");
                int age = Validation.GetInt("Enter the age of the student: ");
                int grade = Validation.GetInt("Enter the student's grade: ");

                // If account info is null, set info 
                if (currentCourse.Students == null)
                {
                    currentCourse.Students = new Student(name, description, age)
                    {
                        Grade = grade
                    };
                }
                else
                {
                    currentCourse.Students.Name = name;
                    currentCourse.Students.Description = description;
                    currentCourse.Students.Age = age;
                    currentCourse.Students.Grade = grade;
                }
            }
        }

        private void DisplayCourseInfo(Course currentCourse)
        {
            if (currentCourse == null)
            {
                Console.WriteLine("Please create a course first.");
            }
            else
            {
                string courseInfo = ($"\nCourse:\n" +
                        $"Title: {currentCourse.Title}\n" +
                        $"Description: {currentCourse.Description}");

                // If course info has been stored then display it
                if (currentCourse != null)
                {
                    Console.WriteLine(courseInfo);
                }

                if (currentCourse.Teacher != null)
                {
                    string teacherInfo = ($"\nTeacher:\nName: {currentCourse.Teacher.Name}\n" +
                        $"Description: {currentCourse.Teacher.Description}\n" +
                        $"Age: {currentCourse.Teacher.Age}\n" +
                        $"Knowledge:");

                    Console.WriteLine(teacherInfo);

                    foreach (string item in currentCourse.Teacher.Knowledge)
                    {
                        Console.WriteLine($"\t{item}");
                    }
                }

                if (currentCourse.Students != null)
                {
                    string studentInfo = ($"\nStudents:\nName: {currentCourse.Students.Name}\n" +
                        $"Description: {currentCourse.Students.Description}\n" +
                        $"Age: {currentCourse.Students.Age}\n" +
                        $"Grade: {currentCourse.Students.Grade}");

                    Console.WriteLine(studentInfo);
                }
            }
        }
    }
}
