﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE03
{
    class Student : Person
    {
        // Create member variables
        private int _grade;

        // Create constructor
        public Student(string name, string description, int age) : base(name, description, age)
        {
        }

        // Encapsulate member variable
        public int Grade { get => _grade; set => _grade = value; }
    }
}
