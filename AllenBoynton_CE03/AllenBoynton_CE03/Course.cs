﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE03
{
    class Course
    {
        // Create member variable
        private string _title;
        private string _description;
        private Teacher _teacher;

        // Needs to be an array but have been unable to create
        private Student _students;

        // Encapsulate member variables
        public string Title { get => _title; set => _title = value; }
        public string Description { get => _description; set => _description = value; }
        internal Teacher Teacher { get => _teacher; set => _teacher = value; }
        internal Student Students { get => _students; set => _students = value; }

        // Constructor for title desc, and number of students
        public Course(string title, string description, Student students)
        {
            _title = title;
            _description = description;
            _students = students;
        }
    }
}
