﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE03
{
    class Teacher : Person
    {
        // Member variable
        private string[] _knowledge;

        // Constructor
        public Teacher(string name, string description, int age) : base(name, description, age)
        {
        }

        // Encapsulate property
        public string[] Knowledge { get => _knowledge; set => _knowledge = value; }
    }
}
