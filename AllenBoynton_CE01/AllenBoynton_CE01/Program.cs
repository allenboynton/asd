﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE01
{
    class Program
    {
        static void Main(string[] args)
        {
            // Allen Boynton
            // ASD 1801 Section #01
            // January 2 2018
            // Code Exercise 01: Big Blue Fish

            // Local variable for number of menu items
            string[] menuItems = new string[] {"Red", "Green", "Blue", "Yellow", "Orange"};
            
            // Create array to contain at least 4 colors - min 12
            string[] fishColorArray = new string[] { "Red", "Green", "Blue", "Yellow",
                "Orange", "Red", "Yellow", "Orange", "Blue", "Yellow", "Orange", "Red", "Blue" };

            // Create an array of floats contains all different lengths of fish - min 12
            float[] lengthOfFish = new float[] { 16.6f, 12.3f, 5.4f, 8.7f, 3.75f, 11.2f, 10.0f,
                8.9f, 13.3f, 18.1f, 14.7f, 9.5f, 6.8f };

            // Integer variable to store user number input
            int userEntryValue;

            // Method call to display menu
            DisplayMenu(menuItems);

            // Store user input
            string userSelection = Console.ReadLine();

            // Conditional to validate the user's number entry
            while (!int.TryParse(userSelection, out userEntryValue) || (userEntryValue < 1) || (userEntryValue > 5))
            {
                // Prompt user to enter a valid number
                Console.WriteLine("You did not enter a valid number.\r\nPlease select a color entry by number 1-5.");
                userSelection = Console.ReadLine();
            }
            
            // Declare a list to store the chosen lengths 
            List<float> lengthAmounts = new List<float>();

            // Loop to iterate through menu colors
            for (int i = 0; i < menuItems.Length; i++)
            {
                // Conditional to collect user's number input of menu item
                if (userEntryValue == i + 1)
                {
                    // Loop through the color array
                    for (int j = 0; j < fishColorArray.Length; j++)
                    {
                        // Compare menu color choice to the color array
                        if (fishColorArray[j].Equals(fishColorArray[i], StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Assign the new value of j to index
                            int index = j;

                            // Use the indices of the matching color array and store the lengths in a List
                            lengthAmounts.Add((float)lengthOfFish.GetValue(index));
                        }
                    }
                    // Store max length from list to a float variable
                    float max = lengthAmounts.Max();
                    int indexNum = Array.IndexOf(lengthOfFish, max);
                    
                    // Display to the console the final length and output
                    Console.WriteLine($"\r\nThe biggest {fishColorArray[i]} fish is fish number {indexNum + 1} " +
                        $"and has a length of {max}.");
                }
            }
            // Prompt user to continue
            Console.WriteLine("\r\nPress any key to continue...");
            Console.ReadKey();
        }

        // Method uses the argument of the menu items to iterate the display
        static void DisplayMenu(string[] menuArray)
        {
            // Create a display to the user to pick a color of fish
            Console.WriteLine("Select a color of fish (1-5):");
            // Loop to make number of menu items reuseable
            for (int i = 0; i < menuArray.Length; i++)
            {
                // Display the menu using string interpolation
                Console.WriteLine($"{i + 1}.{menuArray[i]}");
            }
            Console.Write("\r\nSelect: ");
        }
    }
}
