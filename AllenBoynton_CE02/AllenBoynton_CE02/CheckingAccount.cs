﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE02
{
    class CheckingAccount
    {
        // Create the instances of the account class
        private int _account;
        private decimal _balance;

        // Encapsulate member variables
        public int Account { get => _account; set => _account = value; }
        public decimal Balance { get => _balance; set => _balance = value; }

        // Create a constructor and assign parameters to proper fields
        public CheckingAccount(int account, decimal balance)
        {
            _account = account;
            _balance = balance;
        }
        
        // Constructor for the new balance option
        public CheckingAccount(decimal balance)
        {
            _balance = balance;
        }
    }
}
