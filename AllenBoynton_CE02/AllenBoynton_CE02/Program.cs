﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE02
{
    class Program
    {
        static void Main(string[] args)
        {
            // Required variable created
            Customer currentCustomer = null;
            Program instance = new Program();

            // Bool to continue unless condition becomes false
            bool programIsRunning = true;

            // Conditional to continue program
            while (programIsRunning)
            {
                // Menu per exercise requirements
                Console.Clear();
                Console.WriteLine("Select an action:\n" +
                    "1. Create Customer\n" +
                    "2. Create Account\n" +
                    "3. Set Account Balance\n" +
                    "4. Display Account Information\n" +
                    "5. Exit");
                Console.Write("Teller Options: ");
                string input = Console.ReadLine().ToLower();

                // Switch statement recognizes which choice is picked by the user
                switch (input)
                {
                    case "1":
                    case "create customer":
                        {
                            // Creates currentCustomer
                            currentCustomer = instance.CreateCustomer();
                        }
                        break;
                    case "2":
                    case "create account":
                        {
                            // Instance creates account and balance
                            instance.SetAccount(currentCustomer);
                        }
                        break;
                    case "3":
                    case "set account balance":
                        {
                            // Instance creates balance
                            instance.SetBalance(currentCustomer);
                        }
                        break;
                    case "4":
                    case "display account balance":
                        {
                            // Instance gets account info
                            instance.GetAccountInfo(currentCustomer);
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            // Program is exited
                            programIsRunning = false;
                        }
                        break;
                }

                Utility.DisplayMenuBeforeExiting();
            }
        }

        // Method creates currentCustomer and sets the class variable
        private Customer CreateCustomer()
        {
            Customer newCustomer = null;

            string name = Validation.GetString();
            newCustomer = new Customer(name);

            return newCustomer;
        }

        // Method checks if customer is null and creates account
        private void SetAccount(Customer currentCustomer)
        {
            // Conditional if customer has been added
            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease create a customer first.");
            }
            else
            {
                // If customer was created, user is prompted for account info
                int account = Validation.GetInt();
                decimal balance = Validation.GetDecimal();

                // If account info is null, set info 
                if (currentCustomer.Checking == null)
                {
                    currentCustomer.Checking = new CheckingAccount(account, balance);
                }
                else
                {
                    currentCustomer.Checking.Account = account;
                    currentCustomer.Checking.Balance = balance;
                }
            }
        }

        // Method checks if customer is null and creates a balance
        private void SetBalance(Customer currentCustomer)
        {
            // Conditional if customer has been added
            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease create a customer first.");
            }
            else
            {
                // If customer has value, prompt user for balance if not already existing
                decimal balance = Validation.GetDecimal("\nEnter a new account balance: ");

                if (currentCustomer.Checking == null)
                {
                    currentCustomer.Checking = new CheckingAccount(balance);
                }
                else
                {
                    currentCustomer.Checking.Balance = balance;
                }
            }
        }

        // Method checks if customer is null and displays info to user if not
        private void GetAccountInfo(Customer currentCustomer)
        {
            // Checks if customer has been entered
            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease create a customer first.");
            }
            else
            {
                // If account info has been stored then display it
                if (currentCustomer.Checking == null)
                {
                    Console.WriteLine("No Account...");
                }
                else
                {
                    Console.WriteLine($"\nCustomer: {currentCustomer.GetName()}\n" +
                        $"Checking Account {currentCustomer.Checking.Account} " +
                        $"has a balance of {currentCustomer.Checking.Balance.ToString("C")}");
                }
            }
        }
    }
}
