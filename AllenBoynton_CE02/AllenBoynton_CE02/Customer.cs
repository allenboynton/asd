﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE02
{
    class Customer
    {
        // Instantiate the customer name
        private string _name;
        private CheckingAccount _checking;
        
        // Create a constructor for the customer
        public Customer(string name)
        {
            _name = name;
        }

        // Create getter for member variable
        public string GetName()
        {
            return _name;
        }

        // Create setter for member variable
        public void SetName(string name)
        {
            _name = name;
        }

        public CheckingAccount Checking
        {
            get
            {
                return _checking;
            }
            set
            {
                _checking = value;
            }
        }

        // Method to set the values of CheckingAccount
        public void SetCheckingAccount(CheckingAccount checking)
        {
            _checking = checking;
        }
    }
}
