﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE08
{
    class Delimiter
    {
        private string keyName;
        private string valueName;

        public Delimiter() {}

        public Delimiter(string str) : this()
        {
            string[] stats = str.Split('|');
            keyName = stats[0];
        }

        public override string ToString()
        {
            string retValue = "";
            retValue += ("{\n");
            retValue += ($"\"{KeyName}\" : \"{ValueName}\",\n");
            retValue += ("}\n");

            return retValue;
        }

        public string GetStringToSave()
        {
            string retValue = "";
            retValue = ($"\"{KeyName}\" : \"{ValueName}\"");

            return base.ToString();
        }

        public string KeyName { get => keyName; set => keyName = value; }
        public string ValueName { get => valueName; set => valueName = value; }
    }
}
