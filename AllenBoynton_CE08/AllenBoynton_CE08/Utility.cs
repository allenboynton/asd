﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE08
{
    class Utility
    {
        // Allows user to cycle through the menu and only exit if finished
        public static void DisplayMenuBeforeExiting()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
