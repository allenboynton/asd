﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AllenBoynton_CE08
{
    class Program
    {
        private static List<Delimiter> fieldNames;
        private static List<Delimiter> dataFields;

        static string inputFolder = @"..\..\Input";
        static string outputFolder = @"..\..\Output";

        static void Main(string[] args)
        {
            fieldNames = new List<Delimiter>();
            dataFields = new List<Delimiter>();

            // Make sure directory exists
            Directory.CreateDirectory(outputFolder);
            
            bool programIsRunning = true;

            while (programIsRunning)
            {
                DisplayMenu();

                Console.Write("Selection: ");
                string input = Console.ReadLine().ToLower();
                
                switch (input)
                {
                    case "1":
                    case "datafile1":
                        {
                            LoadData();
                            Load1();
                            Save();
                        }
                        break;
                    case "2":
                    case "datafile2":
                        {
                            LoadData();
                            Load2();
                            Save();
                        }
                        break;
                    case "3":
                    case "datafile3":
                        {
                            LoadData();
                            Load3();
                            Save();
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            programIsRunning = false;
                        }
                        break;
                    default:
                        Console.WriteLine("Command not recognized.");
                        break;
                }

                Utility.DisplayMenuBeforeExiting();
            }
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("Select a file to convert:\n" +
                "1. DataFile 1\n" +
                "2. DataFile 2\n" +
                "3. DataFile 3\n" +
                "4. Exit");
        }

        private static void Save()
        {
            using (StreamWriter outStream = new StreamWriter(outputFolder + @"\ConvertedJsonData.json"))
            {
                foreach (Delimiter data in dataFields)
                {
                    outStream.WriteLine(data.GetStringToSave());
                }
            }
        }

        private static void LoadData()
        {
            using (StreamReader inStream = new StreamReader(inputFolder + @"\DataFieldsLayout.txt"))
            {
                while (inStream.Peek() > -1)
                {
                    string line = inStream.ReadLine();
                    dataFields.Add(new Delimiter(line));
                }

                foreach (Delimiter data in fieldNames)
                {
                    Console.WriteLine("Data Fields: ");
                    Console.WriteLine(data);
                }
            }
        }

        private static void Load1()
        {
            using (StreamReader inStream = new StreamReader(inputFolder + @"\DataFile1.txt"))
            {
                while (inStream.Peek() > -1)
                {
                    string line = inStream.ReadLine();
                    fieldNames.Add(new Delimiter(line));
                }

                foreach (Delimiter data in dataFields)
                {
                    Console.WriteLine("DataFile 1: ");
                    Console.WriteLine(data);
                }
            }
        }

        private static void Load2()
        {
            using (StreamReader inStream = new StreamReader(inputFolder + @"\DataFile2.txt"))
            {
                while (inStream.Peek() > -1)
                {
                    string line = inStream.ReadLine();
                    fieldNames.Add(new Delimiter(line));
                }

                foreach (Delimiter data in dataFields)
                {
                    Console.WriteLine("DataFile 2: ");
                    Console.WriteLine(data);
                }
            }
        }

        private static void Load3()
        {
            using (StreamReader inStream = new StreamReader(inputFolder + @"\DataFile3.txt"))
            {
                while (inStream.Peek() > -1)
                {
                    string line = inStream.ReadLine();
                    fieldNames.Add(new Delimiter(line));
                }

                foreach (Delimiter data in dataFields)
                {
                    Console.WriteLine("DataFile 3: ");
                    Console.WriteLine(data);
                }
            }
        }
    }
}
