﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE06
{
    class CollectionManager
    {
        private List<Card> _cardList = new List<Card>();
        private Dictionary<string, List<Card>> _cardsDictionary = new Dictionary<string, List<Card>>();

        public CollectionManager(List<Card> cardList, Dictionary<string, List<Card>> cardsDictionary)
        {
            _cardList = cardList;
            _cardsDictionary = cardsDictionary;
        }

        public List<Card> CardList { get => _cardList; set => _cardList = value; }
        public Dictionary<string, List<Card>> CardsDictionary { get => _cardsDictionary; set => _cardsDictionary = value; }
    }
}
