﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE06
{
    class Card
    {
        private string _name;
        private string _description;
        private decimal _value;

        public Card(string name, string description, decimal value)
        {
            _name = name;
            _description = description;
            _value = value;
        }

        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }
        public decimal Value { get => _value; set => _value = value; }
    }
}
