﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE06
{
    class Validation
    {
        public static string GetString(string message)
        {
            string validatedString = null;

            do
            {
                Console.Write(message);
                validatedString = Console.ReadLine();
                
            } while (string.IsNullOrWhiteSpace(validatedString));

            return validatedString;
        }

        // Validation for the number entered
        public static int GetInt(string message = "\nEnter a valid number: ")
        {
            int validatedInt;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
                
            } while (!Int32.TryParse(input, out validatedInt));

            return validatedInt;
        }

        public static decimal GetDecimal(string message = "\nEnter a valid number: ")
        {
            decimal validatedDecimal;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!decimal.TryParse(input, out validatedDecimal));

            return validatedDecimal;
        }
    }
}
