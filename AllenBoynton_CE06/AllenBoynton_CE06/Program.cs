﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE06
{
    class Program
    {
        static void Main(string[] args)
        {
            // Required variable created
            CollectionManager currentManager = null;
            Program instance = new Program();

            // Bool to continue unless condition becomes false
            bool programIsRunning = true;

            // Conditional to continue program
            while (programIsRunning)
            {
                // Menu per exercise requirements
                Console.Clear();
                Console.WriteLine("Collection Manager\n" +
                    "--------------------\n" +
                    "1. Create Collection\n" +
                    "2. Create a Card\n" +
                    "3. Add a Card to the Collection\n" +
                    "4. Remove a Card from the Collection\n" +
                    "5. Display a Collection\n" +
                    "6. Display all Collections\n" +
                    "7. Exit");

                Console.Write("Enter your selection: ");
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                        {
                            currentManager = instance.CreateCollection();
                        }
                        break;
                    case "2":
                        {
                            if (currentManager != null)
                            {
                                instance.CreateCard();
                            }
                            else
                            {
                                Console.WriteLine("\nCreate a collection first.\n");
                            }
                        }
                        break;
                    case "3":
                        {
                            if (currentManager != null)
                            {
                                instance.AddCardToCollection();
                            }
                            else
                            {
                                Console.WriteLine("\nCreate a collection first.\n");
                            }
                        }
                        break;
                    case "4":
                        {
                            if (currentManager != null)
                            {
                                instance.RemoveCardFromCollection();
                            }
                            else
                            {
                                Console.WriteLine("\nCreate a collection first.\n");
                            }
                        }
                        break;
                    case "5":
                        {
                            if (currentManager != null)
                            {
                                instance.DisplayCollection();
                            }
                            else
                            {
                                Console.WriteLine("\nCreate a collection first.\n");
                            }
                        }
                        break;
                    case "6":
                        {
                            if (currentManager != null)
                            {
                                instance.DisplayAllColections();
                            }
                            else
                            {
                                Console.WriteLine("\nCreate a collection first.\n");
                            }
                        }
                        break;
                    case "7":
                        {
                            // Program is exited
                            programIsRunning = false;
                        }
                        break;
                }

                Utility.DisplayMenuBeforeExiting();
            }
        }

        private CollectionManager CreateCollection()
        {
            CollectionManager manager = null;

            Console.WriteLine("\nCollection created - Any previous collection items have been lost.\n");
            //manager.CardsDictionary.Add(null, manager.CardList.Add(new Card(null, null, 0m)));

            return manager;
        }

        private Card CreateCard()
        {
            Card newCard = null;

            string name = Validation.GetString("\nEnter the card's name: ");
            string description = Validation.GetString("Enter the card's description: ");
            decimal value = Validation.GetDecimal("Enter the card's value: \n");

            newCard = new Card(name, description, value);

            return newCard;
        }

        private void AddCardToCollection()
        {

        }

        private void RemoveCardFromCollection()
        {

        }

        private void DisplayCollection()
        {

        }

        private void DisplayAllColections()
        {
            
        }
    }
}
