﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE10
{
    class LogToConsole : Logger
    {
        public LogToConsole(int lineNumber) : base(lineNumber)
        {

        }
        
        public override void Log(string message)
        {
            Console.WriteLine($"{LineNumber += 1}, Message: {message}");
        }
    }
}
