﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE10
{
    abstract class Logger : ILog
    {
        private static int _lineNumber;

        public Logger(int lineNumber)
        {
            _lineNumber = lineNumber;
        }

        protected static int LineNumber { get => _lineNumber; set => _lineNumber = value; }

        abstract public void Log(string message);
    }
}
