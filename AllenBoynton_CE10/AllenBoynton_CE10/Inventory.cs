﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE10
{
    class Inventory
    {
        private string _name;
        private string _description;
        private decimal _price;

        public Inventory(string name, string description, decimal price)
        {
            _name = name;
            _description = description;
            _price = price;
        }

        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }
        public decimal Price { get => _price; set => _price = value; }
        
        public string GetTotal(decimal price)
        {
            decimal total = 0m;
            
            total += price; 

            return (total).ToString("C");
        }

        public override string ToString()
        {
            string retValue = "";
            retValue += ($"{Name}\t");
            retValue += ($"Rating: {Description}\t");
            retValue += ($"Price: ${Price}");

            return retValue;
        }
    }
}
