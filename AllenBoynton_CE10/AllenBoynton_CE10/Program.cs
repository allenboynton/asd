﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace AllenBoynton_CE10
{
    class Program
    {
        MySqlConnection connection = null;

        private static List<Inventory> inventory;
        private static List<Customers> customers;
        private static List<Inventory> shoppingCart;

        private static Dictionary<string, Customers> customerDict;

        private static Customers currentCustomer = null;
        private static Inventory currentInventory = null;
        
        static string outputFolder = @"..\..\Output";
        static LogToConsole log;
        
        static void Main(string[] args)
        {
            inventory = new List<Inventory>();
            customers = new List<Customers>();
            shoppingCart = new List<Inventory>();
            customerDict = new Dictionary<string, Customers>();
            
            Program instance = new Program
            {
                connection = new MySqlConnection()
            };
            instance.Connect();

            DataTable data = instance.QueryDB("SELECT DVD_Title, Rating, Price FROM dvd LIMIT 50");
            DataRowCollection rows = data.Rows;

            foreach (DataRow row in rows)
            {
                string name = row["DVD_Title"].ToString();
                string priceString = row["Price"].ToString();
                string rating = row["Rating"].ToString();

                decimal price = 0m;
                while (!decimal.TryParse(priceString, out price))
                {
                    Console.WriteLine("Error converting Numbers.");
                }

                currentInventory = new Inventory(name, rating, price);

                inventory.Add(currentInventory);
            }

            instance.connection.Close();

            Directory.CreateDirectory(outputFolder);

            bool programIsRunning = true;

            while (programIsRunning)
            {
                Console.Clear();
                if (currentCustomer == null)
                {
                    Console.WriteLine($"Current customer: No customer selected\n");
                }
                else
                {
                    Console.WriteLine($"Current customer: {currentCustomer}\n");
                }

                DisplayMenu();
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "Select current customer":
                        {
                            currentCustomer = CreateCustomer();
                            Save();
                        }
                        break;
                    case "2":
                    case "view store inventory":
                        {
                            ViewInventory();
                        }
                        break;
                    case "3":
                    case "view shopping cart":
                        {
                            ViewShoppingCart();
                        }
                        break;
                    case "4":
                    case "add item to cart":
                        {
                            AddItemToCart();
                        }
                        break;
                    case "5":
                    case "remove item from cart":
                        {
                            RemoveItemFromCart();
                        }
                        break;
                    case "6":
                    case "complete purchase":
                        {
                            CompletePurchase();
                        }
                        break;
                    case "7":
                    case "exit":
                        {
                            Console.WriteLine("\n**** Have a nice day :) ****\n");
                            programIsRunning = false;
                        }
                        break;
                    default:
                        Console.WriteLine("Command not recognized.");
                        break;
                }
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void DisplayMenu()
        {
            Console.WriteLine(
                "Generic Store Company\n" +
                "1. Select current customer\n" +
                "2. View store inventory\n" +
                "3. View shopping cart\n" +
                "4. Add item to cart\n" +
                "5. Remove item from cart\n" +
                "6. Complete purchase\n" +
                "7. Exit");
            Console.Write("Select an Option: ");
        }

        private static Customers CreateCustomer()
        {
            Customers newCustomer = null;

            Console.WriteLine("\nSelect current customer: ");
            
            bool selectingCustomer = true;

            while (selectingCustomer)
            {
                Console.Write(
                    "1. New customer\n" +
                    "2. Select an existing customer\n" +
                    "Select a customer option: ");
                string type = Console.ReadLine().ToLower();

                switch (type)
                {
                    case "1":
                    case "new customer":
                        {
                            string name = Validation.GetString("\nCustomer name: ");
                            
                            foreach (Customers c in customers)
                            {
                                while (name.Equals(c.Name, StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("\nThat customer already exists.");
                                    name = Validation.GetString("Please enter a different name: ");
                                }
                            }

                            newCustomer = new Customers(name);
                            customerDict.Add(name, newCustomer);
                            customers.Add(newCustomer);

                            selectingCustomer = false;
                            Utility.DisplayMenuBeforeExiting();
                        }
                        break;
                    case "2":
                    case "select an existing customer":
                        {
                            Console.WriteLine("\nCurrent customers:");

                            int i = 1;
                            foreach (Customers c in customers)
                            {
                                Console.WriteLine($"{i}. {c}");
                                i++;
                            }

                            Console.WriteLine($"{customers.Count + 1}. Exit");
                            int cust = Validation.GetInt("Select a customer: ");

                            if (cust == customers.Count + 1)
                            {
                                Console.WriteLine("\nCurrent customer not changed.");
                                Utility.DisplayMenuBeforeExiting();
                            }
                            else
                            {
                                newCustomer = customers[cust -1];
                            }
                            
                            selectingCustomer = false;
                        }
                        break;
                }
            }

            return newCustomer;
        }

        private static void ViewInventory()
        {
            Console.Clear();
            Console.WriteLine("Current Inventory: ");

            int i = 1;
            foreach (Inventory item in inventory)
            {
                Console.WriteLine($"{i}. {item}");
                i++;
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void ViewShoppingCart()
        {
            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease select a customer first.");
            }
            else
            {
                Console.WriteLine($"\n{currentCustomer}'s cart contents: ");

                int i = 1;
                foreach (Inventory cart in shoppingCart)
                {
                    Console.WriteLine($"{i}. {cart.Name} for ${cart.Price}");
                    i++;
                }
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void AddItemToCart()
        {
            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease select a customer first.");
            }
            else
            {
                bool addItem = true;

                while (addItem)
                {
                    Console.WriteLine("\nAdd an item to the cart: ");

                    int i = 1;
                    foreach (Inventory item in inventory)
                    {
                        Console.WriteLine($"{i}. {item.Name} for ${item.Price}");
                        i++;
                    }
                    
                    int type = Validation.GetInt("Make a selection: ");
                    
                    while (type > inventory.Count || type < 1)
                    {
                        type = Validation.GetInt("\nSelection entered is beyond choices given.\n" +
                            "Please enter a valid selection: ");
                    }
                    
                    currentInventory.Price = inventory[type - 1].Price;

                    shoppingCart.Add(inventory[type - 1]);
                    inventory.Remove(inventory[type - 1]);

                    addItem = false;
                }
            }
        }

        private static void RemoveItemFromCart()
        {
            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease select a customer first.");
            }
            else
            {
                bool removeItem = true;

                while (removeItem)
                {
                    Console.WriteLine("\nRemove item from shopping cart: ");

                    int i = 1;
                    foreach (Inventory item in shoppingCart)
                    {
                        Console.WriteLine($"{i}. {item.Name}");
                        i++;
                    }

                    int type = Validation.GetInt("Make a selection: ");

                    while (type > shoppingCart.Count || type < 1)
                    {
                        type = Validation.GetInt("\nSelection entered is beyond choices given.\n" +
                            "Please enter a valid selection: ");
                    }
                    
                    currentInventory.Price = inventory[type - 1].Price;

                    inventory.Add(shoppingCart[type - 1]);
                    shoppingCart.Remove(shoppingCart[type - 1]);

                    removeItem = false;
                }
            }
        }

        private static void CompletePurchase()
        {
            decimal total = 0m;

            if (currentCustomer == null)
            {
                Console.WriteLine("\nPlease select a customer first.");
            }
            else
            {
                Console.WriteLine($"\nCashing out {currentCustomer}: ");

                int i = 1;
                foreach (Inventory cart in shoppingCart)
                {
                    Console.WriteLine($"{i}. {cart.Name} - Rated {cart.Description} for ${cart.Price}");
                    total += cart.Price;
                    i++;
                }

                Console.WriteLine($"Total: {currentInventory.GetTotal(total)}");
                
                customerDict.Remove(currentCustomer.Name);
                customers.Remove(currentCustomer);
                currentCustomer = null;
                currentInventory = null;
            }

            Utility.DisplayMenuBeforeExiting();
        }

        private static void Save()
        {
            using (StreamWriter outStream = new StreamWriter(outputFolder + @"\log.txt"))
            {
                foreach (Customers c in customers)
                {
                    outStream.WriteLine(c.GetStringToSave());
                }
            }
        }

        private static void Load()
        {
            using (StreamReader inStream = new StreamReader(outputFolder + @"\log.txt"))
            {
                while (inStream.Peek() > -1)
                {
                    string line = inStream.ReadLine();
                    customers.Add(new Customers(line));
                }
            }
        }

        void Connect()
        {
            BuildConnectionString();

            try
            {
                connection.Open();
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + connection.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username/password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }

                Console.WriteLine(msg);
            }
        }

        void BuildConnectionString()
        {
            string ip = "";

            using (StreamReader sr = new StreamReader(@"c:\VFW\connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string connString = $"server={ip};";
            connString += "uid=dbsAdmin;";
            connString += "pwd=password;";
            connString += "database=exampleDatabase;";
            connString += "port=8889";

            connection.ConnectionString = connString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }
    }
}
