﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE10
{
    class Customers
    {
        private string _name;
        
        public string Name { get => _name; set => _name = value; }

        public Customers()
        {

        }

        public Customers(string str) : this()
        {
            string[] stats = str.Split(',');
            _name = stats[0];
        }

        public override string ToString()
        {
            string retVal = "";
            retVal += ($"{Name}");

            return retVal;
        }

        public string GetStringToSave()
        {
            string retValue = "";
            retValue = $"{Name}";

            return base.ToString();
        }
    }
}
