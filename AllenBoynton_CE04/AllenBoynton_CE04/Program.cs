﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE04
{
    class Program
    {
        static void Main(string[] args)
        {
            // Required variable created
            Car currentCar = null;
            Logger logger = null;
            Program instance = new Program();

            bool isLogging = false;
            int lineNumber = 0;
            
            // Bool to continue unless condition becomes false
            bool programIsRunning = true;

            // Conditional to continue program
            while (programIsRunning)
            {
                Console.Clear();

                if (isLogging)
                {
                    Console.WriteLine("Logging is ON\n");
                }
                else
                {
                    Console.WriteLine("Logging is OFF\n");
                }

                // Menu per exercise requirements
                Console.WriteLine("Main Menu\n" +
                    "1. Disable Logging\n" +
                    "2. Enable Logging\n" +
                    "3. Create a Car\n" +
                    "4. Drive the Car\n" +
                    "5. Destroy the Car\n" +
                    "6. Exit");
                Console.Write("Please enter an option: ");
                string input = Console.ReadLine().ToLower();

                // Switch statement recognizes which choice is picked by the user
                switch (input)
                {
                    case "1":
                    case "disable logging":
                        {
                            Console.WriteLine("\nUser chose to disable logging.\n");
                            logger = new DoNotLog(lineNumber);
                        }                        
                        break;
                    case "2":
                    case "enable logging":
                        {
                            logger = new LogToConsole(lineNumber);
                        }
                        break;
                    case "3":
                    case "create a car":
                        {
                            currentCar = instance.CreateCar();                            
                        }
                        break;
                    case "4":
                    case "drive the car":
                        {
                            instance.DriveCar(currentCar);
                        }
                        break;
                    case "5":
                    case "destroy the car":
                        {
                            instance.DestroyCar(currentCar);
                        }
                        break;
                    case "6":
                    case "exit":
                        {
                            // Program is exited
                            programIsRunning = false;
                        }
                        break;
                }

                Utility.DisplayMenuBeforeExiting();
            }
        }

        public static Logger GetLogger()
        {
            Logger logger = null;
            return logger;
        }

        private Car CreateCar()
        {
            Car newCar = null;

            Console.WriteLine("\nUser chose to create a car.\n");
            string make = Validation.GetString("Enter a make of car: ");
            string model = Validation.GetString("Enter a model of car: ");
            string color = Validation.GetString("Enter a color of the car: ");
            float mileage = Validation.GetFloat("Enter the mileage of the car: ");
            int year = Validation.GetInt("Enter a year of the car: ");

            newCar = new Car(make, model, color, mileage, year);

            Console.WriteLine($"\nMake: {newCar.Make}, Model: {newCar.Model}, " +
                $"Color: {newCar.Color}, Year: {newCar.Year}, Mileage: {newCar.Mileage}\n");

            return newCar;
        }

        private void DriveCar(Car currentCar)
        {
            if (currentCar == null)
            {
                Console.WriteLine("\nPlease create a car first.\n");
            }
            else
            {
                Console.WriteLine("\nUser chose to drive the current car.\n");
                float mileage = Validation.GetFloat("Enter a number of miles to drive: ");

                if (currentCar.Mileage == 0)
                {
                    currentCar.Mileage = mileage;
                }
                else
                {
                    currentCar.Drive(mileage);
                }

                Console.WriteLine($"Current Mileage: {currentCar.Mileage}\n");
            }
        }

        private void DestroyCar(Car currentCar)
        {
            if (currentCar == null)
            {
                Console.WriteLine("\nThere is no car to destroy.\nPlease create a car first.\n");
            }
            else
            {
                currentCar = null;

                Console.WriteLine("\nUser chose to destroy the current car.\n");
            }
        }
    }
}
