﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE04
{
    abstract class Logger : ILog
    {
        protected static int _lineNumber;

        public Logger(int lineNumber)
        {
            _lineNumber = lineNumber;
        }

        public int LineNumber
        {
            get
            {
                return _lineNumber;
            }
            set
            {
                _lineNumber = value;
            }
        }

        abstract public void Log(string message);

        abstract public void LogD(string message);

        abstract public void LogW(string message);
    }
}
