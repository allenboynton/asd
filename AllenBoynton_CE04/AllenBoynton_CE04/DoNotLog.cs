﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE04
{
    class DoNotLog : Logger
    {
        public DoNotLog(int lineNumber) : base(lineNumber)
        {

        }
        
        public override void Log(string message)
        {
            
        }

        public override void LogD(string message)
        {
            
        }

        public override void LogW(string message)
        {
            
        }
    }
}
