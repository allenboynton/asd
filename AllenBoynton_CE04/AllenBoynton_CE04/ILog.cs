﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllenBoynton_CE04
{
    interface ILog
    {
        void Log(string message);

        void LogD(string message);

        void LogW(string message);
    }
}
